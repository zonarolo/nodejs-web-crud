const Movie = require('../models/Movie');

const showOneMovie = async (req, res) => {
  try{

    console.log("ESTOY DENTRO DE SHOW-ONE-MOVIE");
    
    // if (req.params.id) {
    //   await Movie.deleteOne({"id": req.params.id}, (err) => {
    //     if (err) throw err;
    //     console.log(`BORRANDO DE LA BASE DE DATOS`);
    //     res.render('movie-delete');
    //   });
    // }
    
    
    if (!req.body.delete) {
      const details = {
        "name": req.body.name,
        "year": req.body.year,
        "id": req.body.id
      }
      await Movie.insertMany(details, (err) => {
        // console.log(details);
        if (err) throw err;
        console.log("200 - INSERCION CORRECTA");
        res.render('movie-details', {movies: details});
      });

    } 
    
    if (req.body.delete) {
      await Movie.deleteOne({"id": req.body.delete}, (err) => {
        if(err) throw err;
        console.log(`200 - DELETE DE ID: ${req.body.delete} CORRECTO`);
        res.render('movie-delete');
      });

    }

    
  } catch (err) {

    console.log(err);
    throw err;
  }
};


const movieList= async (req, res) => {
  try {
    console.log("ESTOY DENTRO DE MOVIE LIST")
    const list = await Movie.find();
    // console.log(list);
    res.render('movie-list', {movies: list});

  } catch (err) {
    console.log(err);
    throw err;
  }
}


module.exports = {
  showOneMovie,
  movieList,
}