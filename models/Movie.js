const mongoose = require('mongoose');

const MovieSchema = new mongoose.Schema({
  id: {
    type: Number,
    index: true,
    unique: true,
    require: true
  },
  name: {
    type: String,
    require: true,
    minlength: 4
  },
  year: {
    type: String,
  }
});

const Movie = mongoose.model('movie', MovieSchema);

module.exports = Movie;