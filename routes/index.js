var express = require('express');
var router = express.Router();

const indexController = require('../controllers/indexController');

/* GET home page. */
// router
// .get('/', indexController.movieList)
// .post('/', indexController.showOneMovie)
// .get('/:id', indexController.showOneMovie)
// .put('/:id', indexController.showOneMovie)
// .delete('/:id', indexController.showOneMovie);

router.route('/movies')
.get(indexController.movieList)
.post(indexController.showOneMovie);

router.route('/movies/:id')
.get(indexController.showOneMovie)
.put(indexController.showOneMovie)
.delete(indexController.showOneMovie);

module.exports = router;
